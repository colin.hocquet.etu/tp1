"use strict";

/*const name = 'Regina';

const url = 'images/'+data.toLowerCase()+'.jpg';
console.log(url);

let html;  
console.log(html);
document.querySelector('.pageContent').innerHTML = html;
*/
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.50,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.50,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.50,
  price_large: 8.00,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
/*
data.forEach(function(value) {
    const url = 'images/'+value.toLowerCase()+'.jpg';
    let img = '<img src="'+url+'"/>';
    let section = '<section>'+value+'</section>';
    html ='<article class="pizzaThumbnail"><a href="'+url+'">'+img+section+'</a></article>';
    document.querySelector('.pageContent').innerHTML += html;
});*/

var html = ""; //Tri alphabétique

/*data.sort(function (a,b){
        return a.name.toLowerCase().localeCompare(b.name.toLowerCase()) ;      
});*/
//Tri prix croissant 

data.sort(function compare(a, b) {
  if (a.price_small < b.price_small) {
    return -1;
  }

  if (a.price_small > b.price_small) {
    return 1;
  } else {
    if (a.price_large < b.price_large) {
      return -1;
    }

    if (a.price_large > b.price_large) {
      return 1;
    }

    return 0;
  }
}); //Tomate
//const result = data.filter(value => value.base == 'tomate');
//prix < 6 
//const result = data.filter(value => value.price_small < 6);
// Deux fois lettre i

function occurs(str) {
  var count = 0;
  var pos = str.indexOf("i");

  while (pos != -1) {
    count++;
    pos = str.indexOf("i", pos + 1);
  }

  return count;
}

var result = data.filter(function (value) {
  return occurs(value.name) == 2;
});
result.forEach(function (value) {
  var url = value.image;
  var img = '<img src="' + url + '"/>';
  var section = '<section><h4>' + value.name + '</h4><ul><li>Prix petit format : ' + value.price_small.toFixed(2) + '</li><li>Prix grand format : ' + value.price_large.toFixed(2) + '</li></ul></section>';
  html += '<article class="pizzaThumbnail"><a href="' + url + '">' + img + section + '</a></article>';
});
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map